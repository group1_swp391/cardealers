﻿using CarDealers.Areas.Admin.Models.OrderAccessoriesViewModel;
using CarDealers.Entity;
using CarDealers.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace CarDealers.Controllers
{
    public class OrderAccessoriesController : CustomController
    {
        private readonly CarDealersContext _context;

        public OrderAccessoriesController(CarDealersContext context)
        {
            _context = context;
            authorizedRoles = new string[] { Constant.CUSTOMER_ROLE };
        }

        [HttpGet]
        public IActionResult CreateOrderAccessories()
        {
            if (!HasAuthorized())
            {
                return RedirectBaseOnPage();
            }
            var viewModel = new CreateOrderAccessoriesViewModel();
            var a = GetCartItems();
            viewModel.Details = a.ToList();
            string customerId = HttpContext.Session.GetString(Constant.LOGIN_USERID_SESSION_NAME);

            if (customerId != null)
            {
                var cus = _context.Customers.FirstOrDefault(x => x.CustomerId == int.Parse(customerId));

                viewModel.FullName = cus.FullName;
                viewModel.Address = cus.Address;
                viewModel.PhoneNumber = cus.PhoneNumber;
                viewModel.Email = cus.Email;

            }
            ViewBag.CustomerId = customerId;
            ViewBag.FooterText = GetDefaultFooterText();
            ViewBag.Menu = GetDefaultMenu();
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateOrderAccessories(CreateOrderAccessoriesViewModel orderA)
        {
            if (!HasAuthorized())
            {
                return RedirectBaseOnPage();
            }
            string customerId = HttpContext.Session.GetString(Constant.LOGIN_USERID_SESSION_NAME);
            var a = GetCartItems();
            var accessories = _context.AutoAccessories.Where(x => x.DeleteFlag == false).ToList();

            orderA.Details = a.ToList();

            if (ModelState.IsValid)
            {
                var guestId = 0;
                if (customerId.IsNullOrEmpty())
                {
                    var customer = new Customer
                    {
                        Email = orderA.Email,
                        PhoneNumber = orderA.PhoneNumber,
                        FullName = orderA.FullName,
                        Address = orderA.Address,
                        CustomerType = 1,
                        CreatedBy = customerId != null ? int.Parse(customerId) : null,
                        CreatedOn = DateTime.Now
                    };
                    await _context.Customers.AddAsync(customer);
                    await _context.SaveChangesAsync();
                    guestId = customer.CustomerId;
                }


                var order = new Order
                {
                    CustomerId = customerId.IsNullOrEmpty() ? guestId : int.Parse(customerId),
                    Status = 0,
                    OrderDate = DateTime.Now,
                    CreatedBy = customerId != null ? int.Parse(customerId) : null,
                    CreatedOn = DateTime.Now,
                };
                await _context.Orders.AddAsync(order);
                await _context.SaveChangesAsync();
                double percent = 0;
                var couponId = 0;
                if (!orderA.CouponCode.IsNullOrEmpty())
                {
                    var coupon = _context.Coupons.FirstOrDefault(e => e.Code.Equals(orderA.CouponCode));
                    if (coupon != null)
                    {
                        couponId = coupon.CouponId;
                        percent = coupon.PercentDiscount;
                    }
                    else
                    {
                        ModelState.AddModelError("CouponCode", "CouponCode is not existed");
                        ViewBag.FooterText = GetDefaultFooterText();
                        ViewBag.Menu = GetDefaultMenu();
                        SaveCartSession(a);
                        return View(orderA);
                    }    
                }

                var orderAccessories = _context.OrderAccessoryDetails.Where(x => x.DeleteFlag == false).ToList();
                var newOrder = a.Select(x => new OrderAccessoryDetail
                {
                    OrderId = order.OrderId,
                    AccessoryId = x.accessory.AccessoryId,
                    SellerId = null,
                    CouponId = couponId == 0 ? null : couponId,
                    Quantity = x.quantity,
                    TotalPrice = (x.accessory.ExportPrice * x.quantity) - ((x.accessory.ExportPrice * x.quantity) * (decimal)percent / 100M),

                    CreatedBy = customerId != null ? int.Parse(customerId) : null,
                    CreatedOn = DateTime.Now
                }).ToList();

                await _context.OrderAccessoryDetails.AddRangeAsync(newOrder);
                await _context.SaveChangesAsync();
                ClearCart();
                return RedirectToAction("YourOrder", "UserProfile");
            }   
            ViewBag.FooterText = GetDefaultFooterText();
            ViewBag.Menu = GetDefaultMenu();
            SaveCartSession(a);
            return View(orderA);
        }

        protected List<News> GetDefaultFooterText()
        {
            var footers = _context.News.Include(e => e.NewsType).Where(e => e.DeleteFlag == false && e.NewsType.NewsTypeName.Equals(Constant.FOOTER)).OrderBy(e => e.Order).ToList();
            return footers;
        }

        protected List<News> GetDefaultMenu()
        {
            var menus = _context.News.Include(e => e.NewsType).Where(e => e.DeleteFlag == false && e.NewsType.NewsTypeName.Equals(Constant.MENU)).OrderBy(e => e.Order).ToList();
            return menus;
        }

        public const string CARTKEY = "cart";

        // Lấy cart từ Session (danh sách CartItem)
        List<CartItem> GetCartItems()
        {
            var session = HttpContext.Session;
            string jsoncart = session.GetString(CARTKEY);
            if (jsoncart != null)
            {
                return JsonConvert.DeserializeObject<List<CartItem>>(jsoncart);
            }
            return new List<CartItem>();
        }

        // Xóa cart khỏi session
        void ClearCart()
        {
            var session = HttpContext.Session;
            session.Remove(CARTKEY);
        }

        // Lưu Cart (Danh sách CartItem) vào session
        void SaveCartSession(List<CartItem> ls)
        {
            var session = HttpContext.Session;
            string jsoncart = JsonConvert.SerializeObject(ls);
            session.SetString(CARTKEY, jsoncart);
        }
    }
}

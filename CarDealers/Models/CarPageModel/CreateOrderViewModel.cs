﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace CarDealers.Models.CarPageModel
{
    public class CreateOrderViewModel
	{
		public int CarId { get; set; }
        public int ColorId { get; set; }
        public IEnumerable<SelectListItem> ColorList { get; set; } = new List<SelectListItem>();
        public string FullName { get; set; } = null!;
		public string CarName { get; set; } = null!;
		public string? CarColor { get; set; }
		public string PhoneNumber { get; set; } = null!;
        public string Email { get; set; } = null!;
		public string? Price { get; set; }
		public string? Address { get; set; }
        public string? DepositePrice { get; set; }
		public DateTime? CreatedOn { get; set; }
	}


}

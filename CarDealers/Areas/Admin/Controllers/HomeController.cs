﻿using CarDealers.Entity;
using CarDealers.Util;
using Microsoft.AspNetCore.Mvc;

namespace CarDealers.Areas.Admin.Controllers
{
    [Area("admin")]
    [Route("admin/[controller]/[action]")]
    public class HomeController : CustomController
    {
        private readonly CarDealersContext _context;

        public HomeController(CarDealersContext context)
        {
            _context = context;
            authorizedRoles = new string[] { Constant.ADMIN_ROLE };
        }
        public IActionResult AdminHomePage()
        {
            if (!HasAuthorized())
            {
                return RedirectBaseOnPage();
            }

            //get total profit
            var totalProfitCars = _context.OrderDetails
                .Where(x => x.Order.Status == 3 && x.Order.DeleteFlag == false)
                .Sum(o => o.TotalPrice ?? 0);
            var totalProfitAccessories = _context.OrderAccessoryDetails
                .Where(x => x.Order.Status == 4 && x.Order.DeleteFlag == false)
                .Sum(o => o.TotalPrice ?? 0);
            ViewBag.totalProfit = (totalProfitCars + totalProfitAccessories).ToString("N0").Replace(",", ".");

            //get total customer
            var totalCustomers = _context.Customers.Where(x => x.DeleteFlag == false).Count();
            ViewBag.totalCustomers = totalCustomers;

            //get total vehicles
            var totalCars = _context.Cars.Where(x => x.DeleteFlag == false).Count();
            ViewBag.totalCars = totalCars;

            //get total order
            var totalOrder = _context.Orders.Where(x => x.DeleteFlag == false).Count();
            ViewBag.totalOrders = totalOrder;

            return View();
        }
    }
}

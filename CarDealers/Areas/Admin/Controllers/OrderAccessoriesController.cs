using CarDealers.Areas.Admin.Models.OrderAccessoriesViewModel;
using CarDealers.Entity;
using CarDealers.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace CarDealers.Areas.Admin.Controllers
{
    [Area("admin")]
    [Route("admin/[controller]/[action]")]
    public class OrderAccessoriesController : CustomController
    {
        private readonly CarDealersContext _context;

        public OrderAccessoriesController(CarDealersContext context)
        {
            _context = context;
            authorizedRoles = new string[] { Constant.ADMIN_ROLE };
        }

        public IActionResult ListOrderAccessories(int? page, int? pageSize, string Keyword, string statusFilter)
        {
            if (!HasAuthorized())
            {
                return RedirectBaseOnPage();
            }
            int defaultPageSize = 10; // Set a default number of records per page
            int pageNumber = page ?? 1;
            int recordsPerPage = pageSize ?? defaultPageSize;
            var orders = _context.Orders.Where(e => e.DeleteFlag == false).ToList();
            var orderSelectList = orders.Select(order => new SelectListItem
            {
                Value = order.OrderId.ToString(),
                Text = order.OrderId.ToString()
            }).ToList();
            ViewBag.OrderOptions = orderSelectList;

            var accessories = _context.AutoAccessories.Where(x => x.DeleteFlag == false).ToList();
            var accessoryTypeSelectList = accessories.Select(accessorie => new SelectListItem
            {
                Value = accessorie.AccessoryId.ToString(),
                Text = accessorie.AccessoryName
            }).ToList();
            ViewBag.AccessoryOptions = accessoryTypeSelectList;

            var coupons = _context.Coupons.Where(x => x.DeleteFlag == false).ToList();
            var couponTypeSelectList = coupons.Select(coupon => new SelectListItem
            {
                Value = coupon.CouponId.ToString(),
                Text = coupon.Name
            }).ToList();
            ViewBag.CouponOptions = couponTypeSelectList;

            var sellers = _context.Users.Where(x => x.DeleteFlag == false).ToList();
            var sellerTypeSelectList = sellers.Select(seller => new SelectListItem
            {
                Value = seller.UserId.ToString(),
                Text = seller.Username
            }).ToList();
            ViewBag.SellerOptions = sellerTypeSelectList;

            var statusList = new UpdateOrderAccessoriesViewModel().StatusList;
            var x = _context.OrderAccessoryDetails.Where(x => x.DeleteFlag == false).ToList();
            var orderaccessories = _context.OrderAccessoryDetails.Where(x => x.DeleteFlag == false)
                .Include(x => x.Order.Customer)
                .Include(x => x.Coupon)
                .Include(x => x.Seller)
                .GroupBy(item => item.CreatedOn)
                .Select(group => group.First())
                .ToList();
            if (!string.IsNullOrEmpty(Keyword))
            {
                var keywords = Keyword.Trim().ToLower().Split(" ").ToList();
                orderaccessories = orderaccessories.Where(u => u.Order.Customer.FullName.ToLower().Contains(Keyword.Trim().ToLower()) || Keyword.Trim().ToLower().Contains(u.Order.Customer.FullName.ToLower()) || keywords.Any(e => u.Order.Customer.FullName.ToLower().Contains(e.Trim().ToLower()))).ToList();
            }
            if (!string.IsNullOrEmpty(statusFilter))
            {
                var status = statusList.FirstOrDefault(e => e.Value.Equals(statusFilter)).Value;
                orderaccessories = orderaccessories.Where(u => u.Order.Status == int.Parse(status)).ToList();
            }
            ViewBag.StatusOptions = statusList;
            ViewBag.Status = statusFilter;
            ViewBag.PageNumber = pageNumber;
            ViewBag.PageSize = recordsPerPage;
            ViewBag.TotalRecords = orderaccessories.Count();
            ViewBag.Keyword = Keyword;
            ViewBag.PageSizeList = new List<int> { 2, 5, 10, 20, 50 }; // Add other values as needed

            var ordernew = orderaccessories?.Select(x => new ListOrderAccessoriesViewModel
            {
                OrderId = x.OrderId,
                FullName = x.Order.Customer.FullName,
                Email = x.Order.Customer.Email,
                PhoneNumber = x.Order.Customer.PhoneNumber,
                Coupon = x.Coupon == null ? null : x.Coupon.Name,
                SellerName = x.SellerId.HasValue ? x.Seller.FullName : null,
                Status = statusList.FirstOrDefault(s => s.Value.Equals(x.Order.Status.ToString())).Text,
                DateOrder = x.CreatedOn.Value.ToString("dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture)
            }).OrderByDescending(e => e.DateOrder).ToList().Skip((pageNumber - 1) * (pageSize.HasValue ? pageSize.Value : defaultPageSize)).Take((pageSize.HasValue ? pageSize.Value : defaultPageSize)).ToList();

            return View(ordernew);
        }

        [HttpGet]
        public IActionResult CreateOrderAccessories()
        {
            var viewModel = new CreateOrderAccessoriesViewModel();
            var accessories = _context.AutoAccessories.Where(x => x.DeleteFlag == false).ToList();
            var coupons = _context.Coupons.Where(x => x.DeleteFlag == false).ToList();

            viewModel.AccessoryList = accessories.Select(accessorie => new SelectListItem
            {
                Value = accessorie.AccessoryId.ToString(),
                Text = accessorie.AccessoryName
            });
            viewModel.CouponList = coupons.Select(coupon => new SelectListItem
            {
                Value = coupon.CouponId.ToString(),
                Text = coupon.Name
            });

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateOrderAccessories(CreateOrderAccessoriesViewModel orderA)
        {
            string customerId = HttpContext.Session.GetString(Constant.LOGIN_USERID_SESSION_NAME);

            var accessories = _context.AutoAccessories.Where(x => x.DeleteFlag == false).ToList();
            var coupons = _context.Coupons.Where(x => x.DeleteFlag == false).ToList();

            orderA.AccessoryList = accessories.Select(accessorie => new SelectListItem
            {
                Value = accessorie.AccessoryId.ToString(),
                Text = accessorie.AccessoryName
            });
            orderA.CouponList = coupons.Select(coupon => new SelectListItem
            {
                Value = coupon.CouponId.ToString(),
                Text = coupon.Name
            });

            if (ModelState.IsValid)
            {
                var customer = new Customer
                {
                    Email = orderA.Email,
                    PhoneNumber = orderA.PhoneNumber,
                    FullName = orderA.FullName,
                    CustomerType = 1,
                    CreatedBy = customerId != null ? int.Parse(customerId) : null,
                    CreatedOn = DateTime.Now
                };
                await _context.Customers.AddAsync(customer);
                await _context.SaveChangesAsync();

                var order = new Order
                {
                    CustomerId = customer.CustomerId,
                    Status = 0,
                    OrderDate = DateTime.Now,
                    CreatedBy = customerId != null ? int.Parse(customerId) : null,
                    CreatedOn = DateTime.Now
                };
                await _context.Orders.AddAsync(order);
                await _context.SaveChangesAsync();

                var select = accessories.Where(x => orderA.SelectedAccessories.Contains(x.AccessoryId));
                var orderAccessories = _context.OrderAccessoryDetails.Where(x => x.DeleteFlag == false).ToList();
                var newOrder = select.Select(x => new OrderAccessoryDetail
                {
                    OrderId = order.OrderId,
                    AccessoryId = x.AccessoryId,
                    CouponId = orderA.CouponId,
                    SellerId = customerId != null ? int.Parse(customerId) : null,
                    Quantity = 1,
                    TotalPrice = x.ExportPrice,

                    CreatedBy = customerId != null ? int.Parse(customerId) : null,
                    CreatedOn = DateTime.Now
                }).ToList();

                await _context.OrderAccessoryDetails.AddRangeAsync(newOrder);
                await _context.SaveChangesAsync();

                return RedirectToAction("ListOrderAccessories");
            }
            return View(orderA);
        }

        [HttpGet]
        public async Task<ActionResult> UpdateOrderAccessories(int id)
        {
            string customerId = HttpContext.Session.GetString(Constant.LOGIN_USERID_SESSION_NAME);
            if (ModelState.IsValid)
            {
                var orderA = _context.OrderAccessoryDetails.Where(x => x.OrderId == id && x.DeleteFlag == false).ToList();

                if (orderA == null)
                {
                    return RedirectToAction("RecordNotFound");
                }

                var order = _context.Orders.Include(x => x.Customer).FirstOrDefault(x => x.OrderId == id);
                var accessories = _context.AutoAccessories.Where(x => x.DeleteFlag == false).ToList();
                var coupons = _context.Coupons.Where(x => x.DeleteFlag == false).ToList();
                var sellers = _context.Users.Where(x => x.DeleteFlag == false).ToList();
                var updateOrderAccessoriesViewModel = new UpdateOrderAccessoriesViewModel
                {
                    OrderId = order.OrderId,
                    FullName = order.Customer.FullName,
                    Email = order.Customer.Email,
                    PhoneNumber = order.Customer.PhoneNumber,
                    CouponId = orderA.FirstOrDefault().CouponId,
                    Status = order.Status.Value,
                    SelectedAccessories = orderA.Select(x => x.AccessoryId),

                    AccessoryList = accessories.Select(accessorie => new SelectListItem
                    {
                        Value = accessorie.AccessoryId.ToString(),
                        Text = accessorie.AccessoryName,
                        Selected = orderA.Select(x => x.AccessoryId).Contains(accessorie.AccessoryId),
                    }),
                    CouponList = coupons.Select(coupon => new SelectListItem
                    {
                        Value = coupon.CouponId.ToString(),
                        Text = coupon.Name,
                        Selected = coupon.CouponId == orderA.FirstOrDefault().CouponId,
                    }),
                };

                updateOrderAccessoriesViewModel.StatusList = updateOrderAccessoriesViewModel.StatusList.Select(x => new SelectListItem
                {
                    Value = x.Value,
                    Text = x.Text,
                    Selected = order.Status.ToString().Equals(x.Value)
                });
                return View(updateOrderAccessoriesViewModel);
            }
            return RedirectToAction("RecordNotFound");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UpdateOrderAccessories(UpdateOrderAccessoriesViewModel updateCarViewModel)
        {
            string customerId = HttpContext.Session.GetString(Constant.LOGIN_USERID_SESSION_NAME);
            var orderAccessoryDetails = _context.OrderAccessoryDetails.Where(x => x.OrderId == updateCarViewModel.OrderId && x.DeleteFlag == false).ToList();

            if (orderAccessoryDetails == null)
            {
                return RedirectToAction("RecordNotFound");
            }

            var order = _context.Orders.Where(x => x.OrderId == updateCarViewModel.OrderId).FirstOrDefault();
            var accessories = _context.AutoAccessories.Where(x => x.DeleteFlag == false).ToList();
            var coupons = _context.Coupons.Where(x => x.DeleteFlag == false).ToList();

            updateCarViewModel.AccessoryList = accessories.Select(accessorie => new SelectListItem
            {
                Value = accessorie.AccessoryId.ToString(),
                Text = accessorie.AccessoryName,
                Selected = updateCarViewModel.SelectedAccessories.Contains(accessorie.AccessoryId),
            });
            updateCarViewModel.CouponList = coupons.Select(coupon => new SelectListItem
            {
                Value = coupon.CouponId.ToString(),
                Text = coupon.Name,
                Selected = coupon.CouponId == updateCarViewModel.CouponId,
            });

            if (updateCarViewModel.Status < order.Status)
            {
                updateCarViewModel.StatusList = updateCarViewModel.StatusList.Select(x => new SelectListItem
                {
                    Value = x.Value,
                    Text = x.Text,
                    Selected = order.Status.ToString().Equals(x.Value)
                });
                updateCarViewModel.Status = (int)order.Status;
                ModelState.AddModelError("Status", "Status cannot turn back the status.");
                return View(updateCarViewModel);
            }

            if (ModelState.IsValid)
            {
                var customer = _context.Customers.FirstOrDefault(x => x.CustomerId == order.CustomerId && x.DeleteFlag == false);
                customer.FullName = updateCarViewModel.FullName;
                customer.PhoneNumber = updateCarViewModel.PhoneNumber;
                customer.Email = updateCarViewModel.Email;
                _context.Customers.Update(customer);
                await _context.SaveChangesAsync();

                order.Status = updateCarViewModel.Status;
                order.ModifiedBy = customerId != null ? int.Parse(customerId) : null;
                order.ModifiedOn = DateTime.Now;
                _context.Orders.Update(order);
                await _context.SaveChangesAsync();
                return RedirectToAction("ListOrderAccessories");
            }
            return View(updateCarViewModel);
        }

        [HttpGet]
        public ActionResult Detail(int id)
        {
            var orderA = _context.Orders.Include(x => x.Customer).FirstOrDefault(x => x.OrderId == id);
            if (orderA == null)
            {
                return RedirectToAction("RecordNotFound");
            }
            var orderViewModel = new ViewOrder
            {
                OrderId = orderA.OrderId,
                FullName = orderA.Customer.FullName,
                PhoneNumber = orderA.Customer.PhoneNumber,
                CreatedOn = orderA.CreatedOn,
                Email = orderA.Customer.Email,
            };

            orderViewModel.OrderAccessories = _context.OrderAccessoryDetails.Where(x => x.OrderId == id && x.DeleteFlag == false)
                .Include(x => x.Coupon)
                .Include(x => x.Accessory)
                .ToList();
            foreach (var item in orderViewModel.OrderAccessories)
            {
                orderViewModel.totalPrice += item.TotalPrice.Value;
            }


            return View(orderViewModel);
        }

        [HttpGet]
        public IActionResult CreateOrderAccessoriesForCustomer()
        {
            var viewModel = new CreateOrderAccessoriesForCustomerViewModel();
            var accessories = _context.AutoAccessories.Where(x => x.DeleteFlag == false).ToList();
            var coupons = _context.Coupons.Where(x => x.DeleteFlag == false).ToList();
            var customers = _context.Customers.Where(x => x.DeleteFlag == false && x.CustomerType == 2).ToList();

            viewModel.AccessoryList = accessories.Select(accessorie => new SelectListItem
            {
                Value = accessorie.AccessoryId.ToString(),
                Text = accessorie.AccessoryName
            });
            viewModel.CustomerList = customers.Select(customer => new SelectListItem
            {
                Value = customer.CustomerId.ToString(),
                Text = customer.FullName
            });
            viewModel.CouponList = coupons.Select(coupon => new SelectListItem
            {
                Value = coupon.CouponId.ToString(),
                Text = coupon.Name
            });

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateOrderAccessoriesForCustomer(CreateOrderAccessoriesForCustomerViewModel orderA)
        {
            string customerId = HttpContext.Session.GetString(Constant.LOGIN_USERID_SESSION_NAME);

            var accessories = _context.AutoAccessories.Where(x => x.DeleteFlag == false).ToList();
            var coupons = _context.Coupons.Where(x => x.DeleteFlag == false).ToList();
            var customers = _context.Customers.Where(x => x.DeleteFlag == false && x.CustomerType == 2).ToList();

            orderA.AccessoryList = accessories.Select(accessorie => new SelectListItem
            {
                Value = accessorie.AccessoryId.ToString(),
                Text = accessorie.AccessoryName
            });
            orderA.CustomerList = customers.Select(customer => new SelectListItem
            {
                Value = customer.CustomerId.ToString(),
                Text = customer.FullName
            });
            orderA.CouponList = coupons.Select(coupon => new SelectListItem
            {
                Value = coupon.CouponId.ToString(),
                Text = coupon.Name
            });

            if (ModelState.IsValid)
            {

                var order = new Order
                {
                    CustomerId = orderA.CustomerId,
                    Status = 0,
                    OrderDate = DateTime.Now,
                    CreatedBy = customerId != null ? int.Parse(customerId) : null,
                    CreatedOn = DateTime.Now
                };
                await _context.Orders.AddAsync(order);
                await _context.SaveChangesAsync();

                var select = accessories.Where(x => orderA.SelectedAccessories.Contains(x.AccessoryId));
                var orderAccessories = _context.OrderAccessoryDetails.Where(x => x.DeleteFlag == false).ToList();
                var newOrder = select.Select(x => new OrderAccessoryDetail
                {
                    OrderId = order.OrderId,
                    AccessoryId = x.AccessoryId,
                    CouponId = orderA.CouponId,
                    SellerId = customerId != null ? int.Parse(customerId) : null,
                    Quantity = 1,
                    TotalPrice = x.ExportPrice,

                    CreatedBy = customerId != null ? int.Parse(customerId) : null,
                    CreatedOn = DateTime.Now
                }).ToList();

                await _context.OrderAccessoryDetails.AddRangeAsync(newOrder);
                await _context.SaveChangesAsync();

                return RedirectToAction("ListOrderAccessories");
            }
            return View(orderA);
        }
        [HttpGet]
        public ActionResult Update(int id)
        {
            var orderA = _context.Orders.Include(x => x.Customer).FirstOrDefault(x => x.OrderId == id);
            if (orderA == null)
            {
                return RedirectToAction("RecordNotFound");
            }
            var orderViewModel = new ViewOrder
            {
                OrderId = orderA.OrderId,
                FullName = orderA.Customer.FullName,
                PhoneNumber = orderA.Customer.PhoneNumber,
                CreatedOn = orderA.CreatedOn,

            };

            orderViewModel.OrderAccessories = _context.OrderAccessoryDetails.Where(x => x.OrderId == id && x.DeleteFlag == false)
                .Include(x => x.Coupon)
                .Include(x => x.Accessory)
                .ToList();
            foreach (var item in orderViewModel.OrderAccessories)
            {
                orderViewModel.totalPrice += item.TotalPrice.Value;
            }


            return View(orderViewModel);
        }

        [HttpPost]
        public ActionResult Update(ViewOrder update)
        {
            var orderAccessoriesDetails = _context.OrderAccessoryDetails
                .Include(x => x.Order.Customer)
                .Include(x => x.Order)
                .Include(x => x.Accessory)
                .Include(x => x.Coupon)
                .Where(x => x.OrderId == update.OrderId && x.DeleteFlag == false).ToList();
            if (orderAccessoriesDetails == null)
            {
                return RedirectToAction("RecordNotFound");
            }



            foreach (var item in orderAccessoriesDetails)
            {
                item.Quantity = update.OrderAccessories.FirstOrDefault(x => x.OrderAccessoryId == item.OrderAccessoryId).Quantity;
                var percent = (decimal)(item.Coupon == null ? 0 : item.Coupon.PercentDiscount);
                item.TotalPrice = (item.Quantity * item.Accessory.ExportPrice) - (item.Quantity * item.Accessory.ExportPrice * (percent / 100));
            }

            _context.OrderAccessoryDetails.UpdateRange(orderAccessoriesDetails);
            _context.SaveChangesAsync();
            var id = update.OrderId;

            return RedirectToAction("Detail", new { id });

        }

        [HttpPost]
        public IActionResult DeleteListOrderAccessories(List<int> selectedIds)
        {
            if (ModelState.IsValid)
            {
                foreach (var id in selectedIds)
                {
                    var order = _context.Orders.Include(e => e.OrderAccessoryDetails).FirstOrDefault(e => e.DeleteFlag == false && e.OrderId == id);
                    if (order != null)
                    {
                        order.DeleteFlag = true;
                        foreach (var item in order.OrderAccessoryDetails)
                        {
                            item.DeleteFlag = true;
                            _context.OrderAccessoryDetails.Update(item);
                        }
                        _context.Orders.Update(order);
                    }
                }
                _context.SaveChanges();
            }
            else
            {
                return RedirectToAction("RecordNotFound");
            }
            return RedirectToAction("ListOrderAccessories");
        }
    }

}

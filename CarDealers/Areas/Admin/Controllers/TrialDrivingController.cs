﻿using CarDealers.Areas.Admin.Models.BookingServiceModel;
using CarDealers.Entity;
using CarDealers.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace CarDealers.Areas.Admin.Controllers
{
    [Area("admin")]
    [Route("admin/[controller]/[action]")]
    public class TrialDrivingController : CustomController
    {
        private readonly CarDealersContext _context;

        public TrialDrivingController(CarDealersContext context)
        {
            _context = context;
            authorizedRoles = new string[] { Constant.ADMIN_ROLE };
        }

        public IActionResult ListTrialDriving(int? page, int? pageSize, string Keyword)
        {
            if (!HasAuthorized())
            {
                return RedirectBaseOnPage();
            }
            int defaultPageSize = 5; // Set a default number of records per page
            int pageNumber = page ?? 1;
            int recordsPerPage = pageSize ?? defaultPageSize;
            var query1 = _context.TrialDrivings.Include(x => x.CarTrail).ThenInclude(x => x.Car).Where(x => x.DeleteFlag == false).ToList();
            if (!string.IsNullOrEmpty(Keyword))
            {
                var keywords = Keyword.Trim().ToLower().Split(" ").ToList();
                query1 = query1.Where(u => u.Email.ToLower().Contains(Keyword.Trim().ToLower()) || Keyword.Trim().ToLower().Contains(u.Email.ToLower()) || keywords.Any(e => u.Email.ToLower().Contains(e.Trim().ToLower()))).ToList();
            }

            var query = query1.AsQueryable();
            var trialDrivings = query.Skip((pageNumber - 1) * (pageSize.HasValue ? pageSize.Value : defaultPageSize)).Take((pageSize.HasValue ? pageSize.Value : defaultPageSize)).ToList();

            ViewBag.PageNumber = pageNumber;
            ViewBag.PageSize = recordsPerPage;
            ViewBag.TotalRecords = query.Count();
            ViewBag.Keyword = Keyword;
            ViewBag.PageSizeList = new List<int> { 2, 5, 10, 20, 50 }; // Add other values as needed

            return View(trialDrivings);
        }

        [HttpGet]
        public ActionResult UpdateTrialDriving(int id)
        {
            if (ModelState.IsValid)
            {
                var trialDriving = _context.TrialDrivings.Include(x => x.CarTrail).ThenInclude(x => x.Car).Where(x => x.TrialId == id && x.DeleteFlag == false).FirstOrDefault();

                if (trialDriving == null)
                {
                    return RedirectToAction("RecordNotFound");
                }
                return View(trialDriving);
            }
            return RedirectToAction("RecordNotFound");
        }

        [HttpPost]
        public ActionResult UpdateTrialDriving(TrialDriving model)
        {
            var trialDriving = _context.TrialDrivings
                .Include(x => x.CarTrail)
                .ThenInclude(x => x.Car)
                .Where(x => x.TrialId == model.TrialId && x.DeleteFlag == false)
                .FirstOrDefault();

            if (trialDriving == null)
            {
                return RedirectToAction("RecordNotFound");
            }

            trialDriving.Status = model.Status;
            trialDriving.ModifiedOn = DateTime.Now;
            _context.TrialDrivings.Update(trialDriving);
            _context.SaveChanges();

            string receiver = trialDriving.Email;
            string subject = "Register Trial Driving Successfully";
            string body = $"You have scheduled a test drive for {trialDriving.CarTrail.Car.Model}<br />Time: {trialDriving.DateBooking.ToString("dd/MM/yyyy")}<br />Plate Number: {trialDriving.CarTrail.PlateNumber}<br />Thanks for using our service!";
            SendMail.SendEmail(receiver, subject, body);
            return RedirectToAction("ListTrialDriving");
        }

        [HttpGet]
        public IActionResult DeleteTrialDriving(int id)
        {
            if (ModelState.IsValid)
            {
                var trialDriving = _context.TrialDrivings.Where(x => x.TrialId == id).FirstOrDefault();
                trialDriving.DeleteFlag = true;
                _context.TrialDrivings.Update(trialDriving);
                _context.SaveChanges();
            }
            else
            {
                return RedirectToAction("RecordNotFound");
            }
            return RedirectToAction("ListTrialDriving");
        }

        [HttpPost]
        public IActionResult DeleteListTrialDriving(List<int> selectedIds)
        {
            if (ModelState.IsValid)
            {
                foreach (var id in selectedIds)
                {
                    var trialDriving = _context.TrialDrivings.Find(id);
                    if (trialDriving != null)
                    {
                        trialDriving.DeleteFlag = true;
                        _context.TrialDrivings.Update(trialDriving);
                    }
                }
                _context.SaveChanges();
            }
            else
            {
                return RedirectToAction("RecordNotFound");
            }
            return RedirectToAction("ListTrialDriving");
        }
    }
}

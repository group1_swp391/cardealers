﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace CarDealers.Areas.Admin.Models.OrderAccessoriesViewModel
{
    public class ListOrderAccessoriesViewModel
    {
        
        public string Email { get; set; } = null!;
        public int OrderId { get; set; }
        public string? Coupon { get; set; }
        public string? SellerName { get; set; }
        public string FullName { get; set; } = null!;
        public string PhoneNumber { get; set; } = null!;
        public string Status { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public string DateOrder { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "0", Text = "Creating an order" },
            new SelectListItem { Value = "1", Text = "Delivering" },
            new SelectListItem { Value = "2", Text = "Wait for pay" },
            new SelectListItem { Value = "3", Text = "Paid" },
            new SelectListItem { Value = "4", Text = "Complete order" }
        };

    }
}

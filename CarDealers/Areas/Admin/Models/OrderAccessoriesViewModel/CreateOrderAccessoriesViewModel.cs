﻿using CarDealers.Entity;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CarDealers.Areas.Admin.Models.OrderAccessoriesViewModel
{
	public class CreateOrderAccessoriesViewModel
	{
        public int? CustomerId { get; set; }
        public string Email { get; set; } = null!;
        public string FullName { get; set; } = null!;
        public string PhoneNumber { get; set; } = null!;
        public string? Address { get; set; }
        public string? CouponCode { get; set; }
        public int OrderId { get; set; }
		public List<CartItem> Details { get; set; } = new List<CartItem>();
		public IEnumerable<SelectListItem> OrderList { get; set; } = new List<SelectListItem>();
		public int AccessoryId { get; set; }
        public IEnumerable<SelectListItem>? AccessoryList { get; set; }
		public IEnumerable<int>? SelectedAccessories { get; set; }
		public int CouponId { get; set; }
		public IEnumerable<SelectListItem> CouponList { get; set; } = new List<SelectListItem>();
		public int? SellerId { get; set; }
		public IEnumerable<SelectListItem> SellerList { get; set; } = new List<SelectListItem>();
		public int Status { get; set; }
		public IEnumerable<SelectListItem> StatusList { get; set; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "0", Text = "Creating an order" },
            new SelectListItem { Value = "1", Text = "Delivering" },
            new SelectListItem { Value = "2", Text = "Wait for pay" },
            new SelectListItem { Value = "3", Text = "Paid" },
            new SelectListItem { Value = "4", Text = "Complete order" }
        };

        public int Quantity { get; set; }

		public decimal? TotalPrice { get; set; }
	
	}

	
}

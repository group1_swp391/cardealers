﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace CarDealers.Areas.Admin.Models.OrderCarViewModel
{
    public class ListOrderCarViewModel
    {
        public string Email { get; set; } = null!;
        public int OrderId { get; set; }
        public string? Coupon { get; set; }
        public string? SellerName { get; set; }
        public string FullName { get; set; } = null!;
        public string PhoneNumber { get; set; } = null!;
        public string Status { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public string DateOrder { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "0", Text = "Deposited" },
            new SelectListItem { Value = "1", Text = "Wait for pay" },
            new SelectListItem { Value = "2", Text = "Paid" },
            new SelectListItem { Value = "3", Text = "Complete the order" }
        };
    }
}
